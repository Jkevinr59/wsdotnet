﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Description;
using System.Web.Services;
using System.Web;
using System.Net;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Security.Permissions;
using System.IO;
using System.Windows.Forms;

namespace WindowsFormsApplication7
{
    class AppController
    {
        private string wsdlpath;
        private string output_path;
        public AppController()
        {

        }
        public void open()
        {
            OpenFileDialog open_dialog = new OpenFileDialog();
            open_dialog.ShowDialog();
            String open_path = open_dialog.FileName;
            wsdlpath = open_path;
        }

        public void save()
        {
            SaveFileDialog save_dialog = new SaveFileDialog();
            save_dialog.ShowDialog();
            String save_path = save_dialog.FileName;
            output_path = save_path;

        }

        public string getfilenames()
        {
            return wsdlpath;
        }

        public void generate(string wsdlPath, string outputFilePath)
        {
            if (File.Exists(wsdlPath) == false)
            {
                return;
            }

            ServiceDescription wsdlDescription = ServiceDescription.Read(wsdlPath);
            ServiceDescriptionImporter wsdlImporter = new ServiceDescriptionImporter();

            wsdlImporter.ProtocolName = "Soap12";
            wsdlImporter.AddServiceDescription(wsdlDescription, null, null);
            wsdlImporter.Style = ServiceDescriptionImportStyle.Server;

            wsdlImporter.CodeGenerationOptions = System.Xml.Serialization.CodeGenerationOptions.GenerateProperties;

            CodeNamespace codeNamespace = new CodeNamespace();
            CodeCompileUnit codeUnit = new CodeCompileUnit();
            codeUnit.Namespaces.Add(codeNamespace);

            ServiceDescriptionImportWarnings importWarning = wsdlImporter.Import(codeNamespace, codeUnit);

            if (importWarning == 0)
            {
                StringBuilder stringBuilder = new StringBuilder();
                StringWriter stringWriter = new StringWriter(stringBuilder);

                CodeDomProvider codeProvider = CodeDomProvider.CreateProvider("CSharp");
                codeProvider.GenerateCodeFromCompileUnit(codeUnit, stringWriter, new CodeGeneratorOptions());

                stringWriter.Close();

                File.WriteAllText(outputFilePath, stringBuilder.ToString(), Encoding.UTF8);

                MessageBox.Show("Code successfully generated. ntaps!!");
            }
            else
            {
                MessageBox.Show(importWarning.ToString());
            } 

         }

        public string get_output_path()
        {
            return output_path;
        }

    }
}
