﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace WindowsFormsApplication7
{
    public partial class Form1 : Form
    {
        AppController Controller = new AppController();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Controller.open();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Controller.generate(Controller.getfilenames(), Controller.get_output_path());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Controller.save();
        }

    }
}
